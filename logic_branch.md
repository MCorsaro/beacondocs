# Logic_Branch

## Functions
**SetValue <*bool*>**
> Sets the value. This will not fire any events even if the value changes

**SetAndTestIfChanged <*bool*>**
> Sets the value, and if it has changed will then run Test

**TestValue**
> Tests the value, and if true will fire `OnTrue` and if false `OnFalse`.

## Events
**OnTrue**

**OnFalse**